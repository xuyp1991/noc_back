export default {
  top_info_exchange: {
    'name': 'noc_top_info',
    'block_queue': 'noc_for_update_blocks',
    'confirm_block_queue': 'noc_for_confirm_blocks'
  },
  block_exchange: {
    'name': 'noc_blocks',
    'transaction_queue': 'noc_for_update_transaction',
  },
  block_exchange_for_confirm: {
    'name': 'noc_blocks_for_confirm',
    'transaction_queue': 'noc_for_update_transaction_for_confirm',
  },
  action_exchange: {
    'name': 'noc_actions',
    'action_queue': ['noc_sys_update_from_actions']
  }
}