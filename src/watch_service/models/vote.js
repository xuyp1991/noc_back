import db_models from '../../db_manage/models'
import {
  EOS
} from './abi_utils'
import {
  excute_sql,
  update_or_create_model
} from '../../db_manage/models'

/*

key                         : Sequelize.STRING,
bpname                      : Sequelize.STRING,
voter                       : Sequelize.STRING,
freeze_block_num            : Sequelize.STRING,
is_has_quited               : Sequelize.BOOLEAN,
unfreeze_block_num          : Sequelize.STRING,
vote                        : Sequelize.STRING,
votepower                   : Sequelize.STRING,
votepower_age               : Sequelize.STRING,
votepower_age_update_height : Sequelize.STRING,
typ                         : Sequelize.INTEGER, // -1 是活期
is_deleted                  : Sequelize.BOOLEAN,

*/

const BASE_VOTE = {
  key                         : 0,
  bpname                      : 0,
  voter                       : 0,
  freeze_block_num            : 0,
  is_has_quited               : 0,
  unfreeze_block_num          : 0,
  vote                        : 0,
  votepower                   : 0,
  votepower_age               : 0,
  votepower_age_update_height : 0,
  typ                         : 0, // -1 是活期
  is_deleted                  : 0,
};

export const update_vote_by_account = async (account_name) => {
  const params = {
    'code': 'bacc',
    'scope': account_name,
    'table': 'votes',
    'lower_bound': 0,
    'limit': 5,
    'json': true
  }

  let more = true, rows = [];
  while(more){
    let data = await EOS.getTableRows(params);
    more = data.more;
    if(more){
      params.lower_bound += data.rows.length;
    }
    rows.splice(0, 0, ...data.rows);
  }

  let saved_fixed_votes = await db_models.fixed_vote.findAll({where: { voter: account_name }})
  
  for(let item of saved_fixed_votes){
    item.is_deleted = true;
    await item.save();
  }

  let row_key = 0;
  for(let row of rows){
    let current_vote = Object.assign({}, BASE_VOTE);
    current_vote.key = -1;
    current_vote.bpname = row.bpname;
    current_vote.voter = account_name;
    current_vote.vote = parseInt(row.vote);
    current_vote.votepower = parseInt(row.votepower);
    current_vote.votepower_age_update_height = row.voteage_update_height;
    current_vote.typ = -1;
    current_vote.is_deleted = 0;
    row_key ++;
    await update_or_create_model('fixed_vote', ['typ', 'bpname', 'voter'], current_vote);

    row.static_votes.forEach(async (sv, id) => {
      sv.key = id;
      sv.bpname = row.bpname;
      sv.voter = account_name;
      sv.vote = parseInt(sv.vote);
      sv.votepower = parseInt(sv.votepower);
      sv.typ = sv.votepower / sv.vote;
      sv.is_deleted = 0;
      await update_or_create_model('fixed_vote', ['key', 'bpname', 'voter'], sv);
    });
  }
}

export const update_vote_users_num = async () => {
  const sql = `select count(distinct(actor)) as vote_num from actions where name in ('vote', 'votefix'); `
  let vote_num = await excute_sql(sql);
  vote_num = vote_num.length > 0 ? vote_num[0].vote_num : 0;
  await update_or_create_model('analytics', 'key', {key: 'total_voted_user_num', value: vote_num});
}


