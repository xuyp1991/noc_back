import Eos from 'eosforcejs'
import config from '../../config'

import {
  wait_time
} from '../../utils/index'

const { eos_node } = config;
export const EOS = Eos(eos_node);

// abi 缓存
const abis = {}; 

const query_abis = async (accounts) => {
  let ps = [];
  for(let i of accounts){
    if(abis[i]) continue;
    ps.push( EOS.getAbi(i) );
  }
  let promise_result = await Promise.all(ps);
  promise_result.forEach(item => {
    abis[item.account_name] = item;
  });
  let result = [];
  accounts.forEach(account => {
    if(abis[account]){
      result.push( abis[account] );
    }
  });
  return result;
}


export const get_value_by_type = async (action, match_types = ['account_name', 'account_name[]']) => {
  let abis = await query_abis([action.act.account]), 
      {structs, tables, actions} = abis[0].abi,
      action_structs = structs.find(i => i.name == action.act.name),
      account_keys = [], 
      result = [];
  match_types = new Set([...match_types]);

  action_structs.fields.forEach(item => {

    if(!match_types.has(item.type) ) return;
    if(typeof action.act.data[item.name] == 'string')
      result.push( {key: item.name, value: action.act.data[item.name]} );
    if(action.act.data[item.name].map && typeof action.act.data[item.name] == 'object'){
      let names = action.act.data[item.name].map(name => {
        return {key: item.name, value: name};
      })
      result.splice(result.length, 0, ...names);
    }

  });
  
  result = [...new Set(result)];
  return result;
}