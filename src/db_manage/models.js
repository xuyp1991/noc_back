import Sequelize from 'sequelize'

import config from '../config'

import structrues from './db_structures'

const { database } = config;

const Model = Sequelize.Model;

const sequelize = new Sequelize(database.db_name, database.name, database.password, {
  host: database.host,
  port: database.port || 3306,
  dialect: 'mysql',
  pool: {
    max: 40,
    min: 5,
    acquire: 30000,
    idle: 10000
  },
  logging: false
});

const models = {};

// init table models
(() => {

  structrues.forEach((item) => {
    models[ item.configs.tableName ] = sequelize.define(item.configs.tableName, item.keys, item.configs);
  });

})();

export const excute_sql = async (sql, is_raw = false) => {
  return new Promise((rs, rj) => {
    sequelize.query(sql, is_raw ? {raw: true} : { type: sequelize.QueryTypes.SELECT})
    .then(data => {
      rs(data);
    });
  });
}

export const model_filter = (table_name, filters = {}) => {
    return new Promise(async (resolve, reject) => {
        let model = models[table_name];
        model.findAll({
            where: filters
        })
        .then((res, error) => {
            resolve(res);
        })
        .catch(error => {
            console.error(error);
            resolve([]);
        })
    });
}

/*
  model_name: String
  unique_keys: [String|Array]
*/
export const update_or_create_model = async (model_name, unique_keys = 'id', value) => {
  unique_keys = unique_keys.slice && unique_keys.splice ? unique_keys : [unique_keys];
  let _model = models[model_name];
  
  let filter_condition = {};
  unique_keys.forEach(row => {
    filter_condition[row] = value[row];
  });
  console.log(filter_condition);
  console.log('filter_condition')
  let result = await _model.findAll({where: filter_condition});

  console.log('filter_condition22')
  if(result.length > 1){
    throw Error({msg: 'unique_key is not right'})
  }
  if(!result.length){
    result = await _model.create(value);
    return result;
  }
  result = result[0];

  let has_changed = false;

  for(let key in value){
    if(value[key] != result[key]){
      has_changed = true;
    }
  }

  if(!has_changed) return result;

  result = Object.assign(result, value);
  if(result) await result.save();
  return result;
}

export const get_lastest = async (model_name, query_keys = 'id') => {
  let _model = models[model_name];
  let result = await _model.findAll({order: [[query_keys, 'DESC']], limit: 1});
  return result[0];
}

export default models;