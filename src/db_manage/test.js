import db_models from './models.js'
import {
  update_or_create_model
} from './models'
import eosforce from 'eosforcejs'

import config from '../config'

const { eos_node: eos_node_config } = config;

const test_get_abi = async () => {
  let eosc = eosforce(eos_node_config);
  let abi = await eosc.getAbi('eosio'),
      info_abi = await eosc.getAbi('bpinfo');

  let fn_fields = abi.abi.structs.find(item => item.name == 'transfer');
  console.log( fn_fields );
  // console.log(abi.abi.structs);
}

// test_get_abi();

const test_create_info_model = async () => {
  let info = await eosforce({httpEndpoint: 'https://w1.eosforce.cn'}).getInfo({});
  let latest = await db_models.info.findAll({
    order: [ ['id', 'desc'] ],
    limit: 1
  });
  console.log(db_models.info);
  // console.log(latest);

  // let result = await db_models.info.create(info)
  // .then(res => {
  //   return res;
  // })
  // console.log(result);
}

// test_create_info_model();

const test_update_or_create_model = async () => {
  let info = await eosforce({httpEndpoint: 'https://w1.eosforce.cn'}).getInfo({});
  info.head_block_num = 12287076;
  let result = update_or_create_model('info', 'head_block_num', info);
}

test_update_or_create_model();
