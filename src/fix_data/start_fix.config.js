module.exports = {
  apps : [
      {
        name        : "set_to_from_bpname_voter",
        script      : "src/fix_data/set_to_from_bpname_vote.js",
        interpreter : "babel-node",
        watch       : false,
        env: {
          "NODE_ENV": "development",
        },
        env_production : {
           "NODE_ENV"   : "production"
        }
      }
  ]
}