import Express from 'express'
import { log, is_int_number } from '../../utils/index.js'
import { 
    get_transaction_total,
    get_bps_from_db,
    get_total_contract,
    get_contract_list,
    get_contract_info_by_account
} from '../models'

import {
    sync_data_api
} from '../settings/common_eos_api.js'

var router = Express.Router();

router.post('/web/get_contract_list', async (req, res) => {
    /*
        params:
            page int
            step int

        response
            {
                status: 0
                data: {
                    total: int
                    page_num: int
                    items: Array
                }
            }
    */
    let form = req.body,

        {page = 1, step = 10} = form,

        base_res = {
            status: 0,
            msg: ''
        };

    if (!is_int_number(page) || !is_int_number(step)) {
        base_res.status = 2;
        base_res.msg = 'page and step must be integer number';
        res.send(base_res);
    }
    let offset = (page - 1) * step;
    let [total, items] = await Promise.all([get_total_contract(), get_contract_list(step, offset)]);
    res.send({
        status: 0,
        data: {
            total,
            total_page_num: Math.ceil(total/step),
            page_num : page,
            items
        }
    });
    
});

router.post('/web/get_contract_info', async (req, resp) => {
    /*
    params:
        account string
    */
    let form = req.body,
        {account} = form,
        base_res = {
            status: 0,
            msg: ''
        };
    if(!account){
        base_res.status = 2;
        base_res.msg = 'no account';
        resp.send(base_res);
        return ;
    }

    base_res.contract_info = await get_contract_info_by_account(account);
    resp.send(base_res);
});

export default router;
