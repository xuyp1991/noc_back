import Express from 'express'
import { log, is_int_number } from '../../utils/index.js'
import { 
    get_total_block_list,
    get_block_list,
    get_accounts_info,
    get_transactions,
    search_eos_key,
    search_accounts,
    query_table_by_condition,
    transfer_dapp_reward_eosc,
    multi_transfer_dapp_reward,
    get_analytics_by_key,
    query_transaction_by_block_time,
    get_total_fixed_vote,
    get_current_vote,
    get_available
} from '../models'

import {
    get_model,
    model_filter,
    excute_sql,
    update_or_insert_data
} from '../../db_manage/models'

import {
    sync_data_api
} from '../settings/common_eos_api.js'

var router = Express.Router();


router.post('/web/get_block_list', async (req, res) => {
    /*
        params:
            page int
            step int

        response
            {
                status: 0
                data: {
                    total: int
                    page_num: int
                    items: Array
                }
            }
    */
    let form = req.body;
    let {page = 1, step = 10} = form;
    let base_res = {
        status: 0,
        msg: ''
    }

    if (!is_int_number(page) || !is_int_number(step)) {

        base_res.status = 2;
        base_res.msg = 'page and step must be integer number';
        res.send(base_res);

    }
    step = 10;
    let total = await get_total_block_list();
    let offset = (page - 1) * step;
    let items = await get_block_list(step, offset);
    res.send({
        status: 0,
        data: {
            total,
            total_page_num: Math.ceil(total/step),
            page_num : page,
            items
        }
    });
    
});


router.post('/web/get_account_info', async (req, res) => {
    /*
        params:
            account_name
        response
            {
                status: 0
                data: {}
            }
    */
    let form = req.body;
    let {account_name} = form;
    let [item, fixed_total, staked, available] = await Promise.all([ 
      get_accounts_info(account_name), 
      get_total_fixed_vote(account_name), 
      get_current_vote(account_name), 
      get_available(account_name)
    ]);
    item.fixed_total = fixed_total;
    item.staked = staked;
    item.available = available;
    res.send({
        status: 0,
        data: item
    });
});

router.post('/web/get_accounts', async (req, res) => {
    /*
        params:
            page int
            step int

        response
            {
                status: 0
                data: {
                    total: int
                    page_num: int
                    items: Array
                }
            }
    */
    let form = req.body,
        {page = 1, step = 10} = form;

    let start_time = new Date().getTime();
    page = parseInt(page);
    step = parseInt(step);
    step = 10;
    let base_res = {
        status: 0,
        msg: ''
    }
    if (!is_int_number(page) || !is_int_number(step)) {
        base_res.status = 2;
        base_res.msg = 'page and step must be integer number';
        res.send(base_res);
    }
    let offset = (page - 1) * step,
        [total, items] = await Promise.all([get_analytics_by_key('ACCOUNT_NUM'), excute_sql(`select * from accounts order by created desc limit ${step} offset ${offset}`)]) ;
    let end_time = new Date().getTime();
    console.log('filter accounts waste' + (end_time - start_time))
    res.send({
        status: 0,
        data: {
            total,
            total_page_num: Math.ceil(total/step),
            page_num : page,
            items
        }
    });
});

router.post('/web/get_transactions', async (req, res) => {
    /*
        params:
            page int
            step int

        response
            {
                status: 0
                data: {
                    total: int
                    page_num: int
                    items: Array
                }
            }
    */
    let form = req.body;
    let {page = 1, step = 10, account_name = ''} = form;

    let base_res = {
        status: 0,
        msg: ''
    }
    step = 10;
    if (!is_int_number(page) || !is_int_number(step)) {

        base_res.status = 2;
        base_res.msg = 'page and step must be integer number';
        res.send(base_res);

    }

    let offset = (page - 1) * step;
    let items = await get_transactions(offset, step);
    let total = await get_analytics_by_key('TRANSACTION_NUM');
    res.send({
        status: 0,
        data: {
            total,
            total_page_num: Math.ceil(total/step),
            page_num: page,
            items
        }
    });
});

router.post('/web/get_transaction', async (req, res) => {
    /*
        params:
            id [transaction id]

        response
            {
                status: 0
                data: {
                    total: int
                    page_num: int
                    items: Array
                }
            }
    */
    let form = req.body;
    let {id} = form;

    let base_res = {
        status: 0,
        msg: ''
    }

    let transaction = await excute_sql(`select * from actions where trx_id = "${id}"`);
    return res.send({
        status: 0,
        data: transaction
    });
});

router.post('/web/get_account_transactions', async (req, res) => {
    /*
        params:
            page int
            step int
            account_name

        response
            {
                status: 0
                data: {
                    total: int
                    page_num: int
                    items: Array
                }
            }
    */
    let form = req.body;
    let {page = 1, step = 10, account_name = ''} = form;
    step = 10;
    let base_res = {
        status: 0,
        msg: ''
    }

    if (!is_int_number(page) || !is_int_number(step)) {

        base_res.status = 2;
        base_res.msg = 'page and step must be integer number';
        res.send(base_res);

    }

    let { modle: transaction_model } = await get_model('transaction');
    let total = await transaction_model.countAsync({});
    let offset = (page - 1) * step;

    let items = await transaction_model.findAll({
            where: {account_name},
            offset,
            limit: step
    })
    res.send({
        status: 0,
        data: {
            total,
            total_page_num: Math.ceil(total/step),
            page_num : page,
            items
        }
    });
});

router.post('/web/search', async (req, res) => {
    /*
        params:
            category ['block', 'accounts', 'transaction', 'eos_public_key']
            key_word

        response
            {
                status: 0
                data: 
            }
    */
    let form = req.body;
    let {category, key_word} = form;
    key_word = key_word.trim();
    let error_msg = {status: 2, msg: '无匹配结果'};
    if(!key_word){
        res.send(error_msg);
        return ;
    }
    // 检测数据类型
    // 是否是 EOS 公钥
    // 检测是否为账号
    log('key_word');
    log(key_word);
    let category_config = [
        {
            name: 'blocks',
            reg_exp: [new RegExp(/^[0-9]*$/), new RegExp(/^[\d|[a-z]{64}$/)],
        },
        {
            name: 'accounts',
            reg_exp: [new RegExp(/^[\d|\.|[a-z]{1,12}$/)]
        },
        {
            name: 'actions',
            reg_exp: [new RegExp(/^[\d|[a-z]{64}$/)]
        },
        {
            name: 'eos_public_key',
            reg_exp: [new RegExp(/^BACC[\d|[A-Za-z]{50}$/)]
        }
    ]
    let search_condition = [], search_condition_map = new Map();
    for(let key_mach of category_config){
        let matched = true;
        for(let reg_mach of key_mach.reg_exp){
            if(!reg_mach.test(key_word)){
                matched = false;
                continue ;
            };
            if(reg_mach.test(key_word)){
                search_condition_map.set(key_mach.name, true);
            }
        }
        if(matched){
            search_condition.push(key_mach.name);
        }
    }
    search_condition = [...search_condition_map.keys()];
    if(!search_condition.length){
        res.send(error_msg);
        return ;
    }
    let search_config = {
            blocks: 'block_num',
            accounts: 'name',
            actions: 'trx_id',
            eos_public_key: 'eos_public_key'
        },
        ps = [];
    for(let item of search_condition){
        if(item == 'accounts'){
            ps.push( search_accounts(key_word) );
        }else if(item == 'eos_public_key'){
            ps.push( search_eos_key(key_word) )
        }else if(item == 'blocks'){
            let condition = key_word.length > 50 ? 'block_id' : 'block_num' ;
            ps.push( query_table_by_condition(item, condition, key_word) );
        }else{
            ps.push( query_table_by_condition(item, search_config[item], key_word) );
        }
    }
    let all_res = await Promise.all(ps), data = [];
    search_condition.forEach((item, index) => {
        if(all_res[index]){
            data.push(item);
        }
    });

    res.send({
        status: 0,
        data
    });
    
});

router.post('/web/search_eth_eos', async (req, res) => {
    /*
        params:
            category ['eth', 'eos_key']
            key_word

        response
            {
                status: 0
                data: []
            }
    */
    let form = req.body;
    let {category, key_word} = form;
    let search_config = {
        eth: 'eth',
        eos_key: 'eos_key'
    }

    if(!search_config[category]){
        res.send({
            status: 2,
            msg: '请选择选项'
        })
        return ;
    }

    let error_msg = {status: 2, msg: '无匹配结果'};

    let condition = {
        [search_config[category]] : key_word
    }
    let items = await model_filter('eth_eos', condition)
    res.send({
        status: 0,
        data: items
    });
});


router.post('/web/get_eos_key_accounts', async (req, res) => {
    /*
        params:
            eos_key
            offset

        response
            {
                status: 0
                data: []
            }
    */
    let form = req.body;
    let { eos_key, offset = 0 } = form;
    let sql = `select distinct(name) from public_key_account where public_key = "${eos_key}" and is_deleted=false limit ${ offset * 20 }, 20`;
    let data = await excute_sql(sql);
    data = data.map(item => item.name);
    res.send({
        status: 0,
        data: {
            account_names: data
        }
    });
});

router.post('/web/get_user_num', async (req, res) => {
    /*
        response
            {
                status: 0
                data: []
            }
    */
    let total = await get_analytics_by_key('ACCOUNT_NUM');
    res.send({
        status: 0,
        data: total
    });
            
});

router.post('/web/get_block', async (req, res) => {
    /*
        params: {
            block_num_or_id: String
        }
        response
            {
                status: 0
                data: {}
            }
    */
    let form = req.body;
    let {block_num_or_id} = form;
    let base_res = {
        status: 2,
        msg: ''
    }
    if(!block_num_or_id){
        res.send(Object.assign(base_res, {
            msg: 'block_num_or_id 不能为空'
        }))
        return ;
    }
    let serch_key = (block_num_or_id + '').length > 50 ? 'block_id' : 'block_num';
    let query_res = await model_filter('blocks', {[serch_key]: block_num_or_id});
    if(!query_res || !query_res.length){
        res.send(Object.assign(base_res, {
            msg: '无当前区块'
        }))
        return ;
    }
    res.send(query_res[0]);
});

router.post('/web/get_analytics', async (req, res) => {
    /*
        账户总数，合约总数，代币总数，投票人数，交易人数 获取
    */
    let result = {
        status: 0,
        data: []
    }
    result.data = await excute_sql(`select * from analytics`);
    // console.log( result.data );
    res.send(result)
});

router.post('/web/get_action', async (req, res) => {
    /*
        params: {
            account_name: String,
            offset: 20,
            pos: 0
        }
    */
    let form = req.body,
        {account_name, offset, pos, pre_id = -1} = form;
    offset = 20;

    let start_time = new Date().getTime();
    let rows1 = await Promise.all(
            [
                query_transaction_by_block_time({key: 'actor', value: account_name, pre_id}),
                query_transaction_by_block_time({key: 'to', value: account_name, pre_id}),
                query_transaction_by_block_time({key: '_from', value: account_name, pre_id}),
            ]
        );
    let rows2 = await Promise.all(
            [
                query_transaction_by_block_time({key: 'voter', value: account_name, pre_id}),
                query_transaction_by_block_time({key: 'bpname', value: account_name, pre_id}),
            ]
        )
    let rows = rows1.concat(rows2);
    let all_ids = [];
    rows.forEach(row => {
        row.forEach(item => {
            all_ids.push(item.id);
        });
    });
    if(!all_ids.length){
        res.send({
            status: 0,
            data: []
        });
        return ;
    }
    let query_all_sql = `select * from actions where id in (${ all_ids.join(',') }) order by block_time desc limit 20`
    let n_r = await excute_sql(query_all_sql);
    let nr = n_r.map(item => {
        let ob = JSON.parse(item.origin_data);
        // ob.block_time = item.block_time;
        // ob.is_confirmed = item.is_confirmed;
        ob = Object.assign(ob, item);
        return ob;
    });
    res.send({
        status: 0,
        data: nr
    });
    return ;

    // let query_id_sql = `select id 
    //                 from transaction where 
    //                 _from = "${account_name}" or 
    //                 \`to\` = "${account_name}" or 
    //                 actor = "${account_name}" or 
    //                 voter = "${account_name}" or 
    //                 bpname = "${account_name}"
    //                 order by block_time desc limit ${offset} offset ${pos}`;
    // let ids = await excute_sql(query_id_sql);
    // let end_time = new Date().getTime();
    // console.log(`${end_time - start_time} ms, in get_action`);

    // if(!ids.length){
    //     res.send({
    //         status: 0,
    //         data: []
    //     });
    //     return ;
    // }
    // let sql = `select *
    //             from transaction where 
    //             id in (${ ids.map(item => item.id).join(',') }) order by block_time desc`;

    // let query_res = await excute_sql(sql);
    // let result = query_res.map(item => {
    //     let ob = JSON.parse(item.origin_data);
    //     // ob.block_time = item.block_time;
    //     // ob.is_confirmed = item.is_confirmed;
    //     ob = Object.assign(ob, item);
    //     return ob;
    // });
    // res.send({
    //     status: 0,
    //     data: result
    // });
});

router.post('/web/get_action_of_contract', async (req, res) => {
    /*
        params: {
            account_name: String,
            offset: 20,
            pos: 0
        }
    */
    let form = req.body;
    let {account_name, offset, pos} = form;
    offset = 20;
    let query_id_sql = `select id 
                    from actions where 
                    account = "${account_name}"
                    order by block_time desc limit ${offset} offset ${pos}`;
    let ids = await excute_sql(query_id_sql);
    if(!ids.length){
        res.send({
            status: 0,
            data: []
        });
        return ;
    }
    let sql = `select *
                from actions where 
                id in (${ ids.map(item => item.id).join(',') }) order by block_time desc`;

    let query_res = await excute_sql(sql);
    let result = query_res.map(item => {
        let ob = JSON.parse(item.origin_data);
        // ob.block_time = item.block_time;
        // ob.is_confirmed = item.is_confirmed;
        ob = Object.assign(ob, item);
        return ob;
    });
    res.send({
        status: 0,
        data: result
    });
});

router.post('/web/get_action_of_token', async (req, res) => {
    /*
        params: {
            symbol: String,
            offset: 20,
            pos: 0
        }
    */
    let form = req.body;
    let {symbol, offset, pos} = form;
    offset = 20;
    let query_id_sql = `select id, token_type, block_time from transaction where token_type = "${symbol}" order by block_time desc limit ${offset} offset ${pos}`;
    console.log('get_action_of_token', query_id_sql);
    let ids = await excute_sql(query_id_sql);
    if(!ids.length){
        res.send({
            status: 0,
            data: []
        });
        return ;
    }
    let sql = `select *
                from transaction where 
                id in (${ ids.map(item => item.id).join(',') }) order by block_time desc`;

    let query_res = await excute_sql(sql);
    let result = query_res.map(item => {
        let ob = {};
        try{
            ob = JSON.parse(item.origin_data)
        }catch(e){

        }
        ob = Object.assign(ob, item);
        return ob;
    });
    res.send({
        status: 0,
        data: result
    });
});

router.post('/web/get_vote_num', async (req, res) => {
    /*
        params: {}
    */
    let form = req.body;
    let {account_name, offset, pos} = form;
    let sql = `select * from analytics where \`key\` = 'total_voted_user_num'`;
    let query_res = await excute_sql(sql);
    res.send({
        status: 0,
        data: query_res.length ? query_res[0].value : 0
    });
});

router.post('/web/add_blog', async (req, res) => {
    /*
        params: {
            author,
            title,
            illustration,
            brief,
            content,
            publish_time,
            is_published = true,
            password
        }
    */
    let form = req.body;
    if(!form.password || form.password != 'hello_publish_it'){
        res.send({
            status: 2,
            msg: 'no password'
        });
        return ;
    }
    let blog = {};
    let {model, keys} = await get_model('blog');
    keys.forEach(item => blog[item] = form[item] );
    blog.is_published = blog.is_published == 'true' ? true : false;
    console.log( blog.publish_time );
    blog.publish_time = new Date();
    if (blog.id) {
        let items = await model_filter('blog', {id: blog.id})
        model._db_close();
        if (!items.length) {
            res.send({
                status: 2,
                data: 'not existed'
            });
            return ;
        }

        keys.forEach(item => {
            items[0].set(item, blog[item]);
        });

        items[0].save();
        res.send({
            'status': 0,
            'data': blog
        });

        return ;
    }

    try{
        
        delete blog.id;
        blog = await model.createAsync(blog);
    }catch(e){
        res.send({
            status: 2,
            data: e.cause.sqlMessage
        });
        return ;
    }
    model._db_close();
    res.send({
        status: 0,
        data: blog
    });
});

router.post('/web/get_blog_brief_list', async (req, res) => {
    /*
        params: {
            pre int
        }
    */
    let form = req.body;
    let { pre = 0, get_max = false} = form;
    log( typeof get_max );
    let where_condition = get_max ? 'where id > ' : 'where id < ';
    let query_sql = `
                    select id, author, title, illustration, brief, publish_time
                    from blog ${ pre ? where_condition + pre : ''} 
                    order by id desc limit 10
                    `;
    let blog_list = [];
    try{
        blog_list = await excute_sql(query_sql);
    }catch(e){
        res.send({
            status: 2,
            data: e
        });
        return ;
    }
    res.send({
        status: 0,
        data: blog_list
    });
});

router.post('/web/get_blog_item', async (req, res) => {
    /*
        params: {
            id int
        }
    */
    let form = req.body;
    let { id } = form;
    let query_sql = `
                    select id, author, title, illustration, brief, content, publish_time 
                    from blog where id = ${id}
                    `;
                    
    let blog_item = [];
    try{
        blog_item = await excute_sql(query_sql);
    }catch(e){
        res.send({
            status: 2,
            data: e
        });
        return ;
    }

    res.send({
        status: 0,
        data: blog_item[0]
    });
});

router.get('/web/total_amount', async (req, res) => {
    let info = await sync_data_api.get_info();
    let total = info.head_block_num * 10 + 1000000000;
    res.send(total + '');
});

router.post('/web/get_user_reward', async (req, res) => {
    /*
        params: {
            user_name
        }
    */
    // dapp_reward
    let form = req.body;
    let { user_name } = form;
    if(!user_name){
        res.send({
            status: 2,
            msg: '用户未登录'
        })
        return ;
    }
    res.send({
        status: 3,
        msg: '活动已结束'
    });
    return ;
    let now = new Date();
    let today = new Date(`${ now.getFullYear() }/${ now.getMonth() + 1 }/${ now.getDate() } 00:00:00`);
    let date_reward = await model_filter('dapp_reward_limit', {date_time: today});
    date_reward = date_reward.length ? date_reward[0] : update_or_insert_data('dapp_reward_limit', {date_time: today, max_limit: 1000, has_sended: 0 }, 'date_time');
    let my_dapp_reward = await model_filter('dapp_reward', {user_name}),
        not_recieved = 0,
        can_recieved = 0,
        total = 0;

    for(let item of my_dapp_reward){
        item.has_revived ? null : not_recieved += item.reward;
        total += item.reward;
    }
    can_recieved = date_reward.max_limit - date_reward.has_sended > not_recieved ? not_recieved : date_reward.max_limit - date_reward.has_sended;
    can_recieved = can_recieved > 0 ? can_recieved : 0;
    /* remove get reward*/
    res.send({
        status: 0,
        data: {
            date_reward,
            my_dapp_reward,
            can_recieved,
            not_recieved,
            total
        }
    })
});

router.post('/web/get_my_dapp_reward', async (req, res) => {
    /*
        params: {
            user_name
        }
    */
    // dapp_reward
    let form = req.body;
    let { user_name } = form;
    if(!user_name){
        res.send({
            status: 2,
            msg: '用户未登录'
        })
        return ;
    }
    res.send({
        status: 3,
        msg: '活动已结束'
    });
    return ;

    let now = new Date();
    let today = new Date(`${ now.getFullYear() }/${ now.getMonth() + 1 }/${ now.getDate() } 00:00:00`);
    let date_reward = await model_filter('dapp_reward_limit', {date_time: today});
    date_reward = date_reward.length ? date_reward[0] : update_or_insert_data('dapp_reward_limit', {date_time: today, max_limit: 1000, has_sended: 0 }, 'date_time');
    let my_dapp_reward = await model_filter('dapp_reward', {user_name}),
        not_recieved = 0,
        can_recieved = 0,
        total = 0,
        memo = 'DAPP REWARD:';

    await multi_transfer_dapp_reward(user_name, date_reward, my_dapp_reward);
    for(let item of my_dapp_reward){
        item.has_revived ? null : not_recieved += item.reward;
        total += item.reward;
    }
    can_recieved = date_reward.max_limit - date_reward.has_sended > not_recieved ? not_recieved : date_reward.max_limit - date_reward.has_sended;
    can_recieved = can_recieved > 0 ? can_recieved : 0;
    // 
    // 
    res.send({
        status: 0,
        data: {
            date_reward,
            my_dapp_reward,
            can_recieved,
            not_recieved,
            total
        }
    })
});

// 
export default router;











