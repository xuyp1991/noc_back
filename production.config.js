module.exports = {
  apps : [
      {
        name        : "watch_action_for_service",
        script      : "src/watch_service/watch_actions_for_service.js",
        interpreter : "babel-node",
        watch       : false,
        env: {
          "NODE_ENV": "development",
        },
        env_production : {
           "NODE_ENV"   : "production"
        }
      },
      {
        name        : "watch_transaction",
        script      : "src/watch_service/watch_transactions.js",
        interpreter : "babel-node",
        watch       : false,
        env: {
          "NODE_ENV": "development",
        },
        env_production : {
           "NODE_ENV"   : "production"
        }
      },
      {
        name        : "watch_block",
        script      : "src/watch_service/watch_block.js",
        interpreter : "babel-node",
        watch       : false,
        env: {
          "NODE_ENV": "development",
        },
        env_production : {
           "NODE_ENV"   : "production"
        }
      },
      {
        name        : "watch_confirmed_block",
        script      : "src/watch_service/watch_confirmed_block.js",
        interpreter : "babel-node",
        watch       : false,
        env: {
          "NODE_ENV": "development",
        },
        env_production : {
           "NODE_ENV"   : "production"
        }
      },
      {
        name        : "watch_info",
        script      : "src/watch_service/watch_info.js",
        interpreter : "babel-node",
        watch       : false,
        env: {
          "NODE_ENV": "development",
        },
        env_production : {
           "NODE_ENV"   : "production"
        }
      },
      {
        name        : "web",
        script      : "src/web/init_app.js",
        interpreter : "babel-node",
        watch       : false,
        env: {
          "NODE_ENV": "development",
        },
        env_production : {
           "NODE_ENV"  : "production",
           "NODE_PORT" : 3003
        }
      },
      {
        name        : "watch_all_service",
        script      : "./src/watch_process/index.js",
        interpreter : "babel-node",
        watch       : true,
        env: {
          "NODE_ENV": "production",
        },
        env_production : {
           "NODE_ENV"   : "production",
        },
      }
  ]
}