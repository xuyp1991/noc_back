module.exports = {
  apps : [
      {
        name        : "fix_action_by_block_num",
        script      : "src/watch_service/fix_action_by_block_num.js",
        interpreter : "babel-node",
        watch       : false,
        env: {
          "NODE_ENV": "development",
        },
        env_production : {
           "NODE_ENV"   : "production"
        }
      }
  ]
}